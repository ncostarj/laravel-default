<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class MenusProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('menus_profiles')->insert([
        		['menu_id'=>1,
        		'profile_id' =>1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()],
        		['menu_id'=>2,
        		'profile_id' =>1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()],
        		['menu_id'=>3,
        		'profile_id' =>1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()],
        	]); 
    }
}
