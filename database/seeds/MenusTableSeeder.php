<?php

use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('menus')->insert([
 			[
            'name' => 'Perfis',
            'icon' => 'fa fa-users',
            'location' => 'profiles',
            'status' => 1,
        	],
 			[
            'name' => 'Menus',
            'icon' => 'fa fa-tasks',
            'location' => 'menus',
            'status' => 1,
        	],
			[
            'name' => 'Usuários',
            'icon' => 'fa fa-user',
            'location' => 'users',
            'status' => 1,
        	],
        ]);
    }
}
