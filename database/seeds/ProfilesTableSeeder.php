<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class ProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('profiles')->insert([
			[
	            'name'        => 'Administrador',
	            'description' => 'Perfil de administração do sistema.',
	            'created_at'  => Carbon::now(),
	            'updated_at'  => Carbon::now(),
        	],
        	[
	            'name'        => 'Usuário',
	            'description' => 'Perfil de usuário do sistema.',
	            'created_at'  => Carbon::now(),
	            'updated_at'  => Carbon::now(),
        	]
        ]);
    }
}
