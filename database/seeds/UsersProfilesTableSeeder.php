<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users_profiles')->insert([
			[
	            'user_id'    => 1,
	            'profile_id' => 1,
	            'created_at' => Carbon::now(),
	            'updated_at' => Carbon::now(),
        	],
        	[
	            'user_id'    => 1,
	            'profile_id' => 2,
	            'created_at' => Carbon::now(),
	            'updated_at' => Carbon::now(),
        	]
        ]);
    }
}
