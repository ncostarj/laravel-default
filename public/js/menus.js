$(document).ready(function(){
    $('#todos').click(function(){
        if($(this).is(':checked'))
        {
            $('.ck-item').prop('checked',true);
        }
        else
        {
            $('.ck-item').prop('checked',false);   
        }
    });

    $('#destroy-many').submit(function(){
        if($('.ck-item:checked').length > 0)
        {
            $ids = '';
            $('.ck-item').each(function(){
                if($(this).is(':checked')){
                    $ids += $(this).val() + ',';    
                } 
            });

            $ids = $ids.substr(0,($ids.length-1));
            
            $('#menus').val($ids);

            return true;   
        }
        else
        {
            alert('Por favor selecione os perfis que deseja enviar para a lixeira!');
            return false;
        }
        
    });
});