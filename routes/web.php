<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');
Route::get('/profiles/trash','ProfileController@showTrash')->name('profiles.trash');
Route::get('/profiles/{id}/restore','ProfileController@restore')->name('profiles.restore');
Route::get('/profiles/{id}/delete','ProfileController@delete')->name('profiles.delete');
Route::post('/profiles/destroy-many','ProfileController@destroyMany')->name('profiles.destroy-many');
Route::resource('/profiles','ProfileController');
Route::get('/menus/trash','MenuController@showTrash')->name('menus.trash');
Route::get('/menus/{id}/restore','MenuController@restore')->name('menus.restore');
Route::get('/menus/{id}/delete','MenuController@delete')->name('menus.delete');
Route::get('/menus/{menu}/activate','MenuController@activate')->name('menus.activate');
Route::get('/menus/{menu}/deactivate','MenuController@deactivate')->name('menus.deactivate');
Route::post('/menus/destroy-many','MenuController@destroyMany')->name('menus.destroy-many');
Route::resource('/menus','MenuController');
