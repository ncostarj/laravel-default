<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $menus = $this->getMenuList();

        $trash = 0;

        return view('menus.index')->with(compact('menus','trash'));        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menus   = Menu::whereNull('parent_id')->get();

        $parents = $this->getMenuOptions($menus);

        return view('menus.save')->with(compact('menus','parents'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'icon'     => 'required',
            'name'     => 'required',
            'location' => 'required',
            'status'   => 'required',
        ]);        
        
        $menu = new Menu;
        $menu->icon     = $request->icon;
        $menu->name     = $request->name;
        $menu->location = $request->location;
        $menu->status   = $request->status;

        if($request->has('parent')){
            $menu->parent()->associate($request->parent);
        }

        $menu->save();

        return redirect('menus')->with(['success'=>'Menu salvo com sucesso.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function show(Menu $menu)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        $menus   = Menu::whereNull('parent_id')->get();
        
        $parents = $this->getMenuOptions($menus);

        return view('menus.save')->with(compact('menu','menus','parents'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        $this->validate($request, [
            'icon'     => 'required',
            'name'     => 'required',
            'location' => 'required',
            'status'   => 'required',
        ]);

        $menu->icon     = $request->icon;
        $menu->name     = $request->name;
        $menu->location = $request->location;
        $menu->status   = $request->status;

        if($request->has('parent')){
            $menu->parent()->associate($request->parent);
        }

        $menu->save();

        return redirect('menus')->with(['success'=>'Menu salvo com sucesso.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();
        return redirect('menus');
    }

    public function destroyMany(Request $request)
    {
        Menu::destroy(explode(',',$request->menus));
        return redirect('menus')->with(compact('menus','trash'))->with(['success'=>'Menu(s) foi(ram) enviados para a lixeira com sucesso.']); 
    }

    public function showTrash()
    {
        $menus = Menu::onlyTrashed()->get();
        $trash = 1;
        return view('menus.index')->with(compact('menus','trash'));        
    }

    public function restore($id)
    {
        Menu::withTrashed()->where('id', $id)->restore();
        $trash = 0;
        return redirect('menus')->with(compact('menus','trash'))->with(['success'=>'Menu foi jogado na lixeira.']);        
    }

    public function delete($id)
    {
        Menu::withTrashed()->where('id', $id)->forceDelete();
        $trash = 0;
        return redirect('menus')->with(compact('menus','trash'))->with(['success'=>'Menu foi apagado com sucesso.']);        
    }

    public function activate(Menu $menu){
        $menu->status = 1;
        $menu->save();
        return redirect('menus');
    }

    public function deactivate(Menu $menu){
        $menu->status = 0;
        $menu->save();
        return redirect('menus');
    }

    public function getMenuOptions($menus)
    {
        $parents = array();

        foreach($menus as $menu)
        {
            $parents[$menu->id] = $menu->name;
            $children = $menu->recursiveMenusOptions($menu, $menu->name);
            if(!empty($children))
            {
                foreach($children as $i => $child)
                {
                    $parents[$i] = $child;
                }
            }
        }

        return $parents;        
    }

    public function getMenuList()
    {

        $menus = Menu::whereNull('parent_id')->get();

        $parents = array();
        foreach($menus as $menu)
        {
            $parents[$menu->id] = $menu;
            $children = $menu->getChildren($menu, $menu->name);
            if(!empty($children))
            {
                foreach($children as $i => $child)
                {
                    $parents[$i] = $child;
                }
            }
        }

        return collect($parents);
    }
}
