<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use App\Models\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $profiles = Profile::all();
        $trash = 0;
        return view('profiles.index')->with(compact('profiles','trash'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $menus = Menu::all();
        return view('profiles.save')->with(compact('menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'name'         => 'required',
            'description'  => 'required',
        ]);        

        $profile = Profile::create(['name' => $request->name,'description' => $request->description]);
        $profile->menus()->sync($request->menus);

        return redirect('profiles')->with(['success'=>'Perfil salvo com sucesso.']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function show(Profile $profile)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function edit(Profile $profile)
    {
        $menus = Menu::all();
        return view('profiles.save')->with(compact('profile','menus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profile $profile)
    {
        $this->validate($request, [
            'name'         => 'required',
            'description'  => 'required',
        ]);        

        $$profile = $profile->fill(['name' => $request->name,'description' => $request->description]);
        $profile->save();
        $profile->menus()->sync($request->menus);

        return redirect('profiles')->with(['success'=>'Perfil salvo com sucesso.']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profile $profile)
    {
        //
        $profile->delete();

        return redirect('profiles')->with(['success'=>'Perfil foi enviado para lixeira.']);
    }

    public function destroyMany(Request $request)
    {
        Profile::destroy(explode(',',$request->profiles));
        return redirect('profiles')->with(compact('profiles','trash'))->with(['success'=>'Perfil(is) foi(ram) enviados para a lixeira com sucesso.']); 
    }

    public function showTrash()
    {
        $profiles = Profile::onlyTrashed()->get();
        $trash = 1;
        return view('profiles.index')->with(compact('profiles','trash'));        
    }

    public function restore($id)
    {
        Profile::withTrashed()->where('id', $id)->restore();
        $trash = 0;
        return redirect('profiles')->with(compact('profiles','trash'))->with(['success'=>'Perfil foi jogado na lixeira.']);        
    }

    public function delete($id)
    {
        Profile::withTrashed()->where('id', $id)->forceDelete();
        $trash = 0;
        return redirect('profiles')->with(compact('profiles','trash'))->with(['success'=>'Perfil foi apagado com sucesso.']);        
    }
}
