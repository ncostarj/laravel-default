<?php

namespace App\Http\Middleware;

use App\Models\Menu;
use Closure;
use Illuminate\Support\Facades\DB;

class SecurityCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $location = $request->segment(1);

        $user = auth()->user();

        if($user)
        {
                $menus = array();

                foreach($user->profiles as $profile)
                {
                    foreach($profile->menus as $menu)
                    {
                        $menus[] = $menu->toArray();
                    }
                }

                $permission = collect($menus);

                if($location != 'dashboard' && $location != 'logout')
                {
                    if(!$permission->contains('location', $request->segment(1)))
                    {
                        $error = 'Seu usuário não tem permissão para acessar esta área!';
                        return redirect('dashboard')
                        ->with(compact('error'));
                    }
                    else
                    {
                        return $next($request);       
                    }
                }
                else 
                {
                    return $next($request);
                }    
        }
        else
        {
            return $next($request);
        }

    }
}
