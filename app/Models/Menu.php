<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use SoftDeletes;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['icon','name','location','status'];

    public function children()
    {
    	return $this->hasMany('App\Models\Menu','parent_id');
    }

    public function parent()
    {
    	return $this->belongsTo('App\Models\Menu','parent_id');
    }

    public function getHasChildrenAttribute()
    {
    	$return = false;
    	if($this->children->count() > 0)
    	{
    		$return = true;
    	}
    	return $return;
    }

    public function recursiveMenusOptions($menu, $titulo){
	    $arrSubmenu = array();
	    if($menu->children->count() > 0)
	    {
	        $submenu = $menu->children;	        
	        foreach($submenu as $sub)
	        {
	            $newTitulo = $titulo . ' &raquo; ' . $sub->name;
	            $arrSubmenu[$sub->id] = $newTitulo;
				$arrSubmenu += $this->recursiveMenusOptions($sub,$newTitulo);
	        }
	    }
	    return $arrSubmenu;
   	}

    public function getChildren($parent, $title){
	    $arrChildren = array();
	    if($parent->has_children)
	    {
	        $children = $parent->children;	        
	        foreach($children as $child)
	        {
	        	$newTitle = $title . ' &raquo; ' . $child->name;
	        	$menu = new Menu;
	        	$menu->id = $child->id;
	        	$menu->parent_id = $child->parent_id;
	        	$menu->icon = $child->icon;
	        	$menu->name = $newTitle;
	        	$menu->location = $child->location;
	        	$menu->status  = $child->status;
	        	$menu->created_at = $child->created_at;
	        	$menu->updated_at = $child->updated_at;
	        	$menu->deleted_at = $child->deleted_at;

				$arrChildren[$menu->id] = $menu;
				$arrChildren += $this->getChildren($menu, $menu->name);
	        }
	    }
	    return $arrChildren;
   	}
}
