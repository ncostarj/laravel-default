@extends('layouts.app')
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/menus.js') }}"></script>
@endsection
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('common.messages')
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-heading">

                    @if($trash)
                        <button type="button"><a href="{{ url('menus') }}"><i class="fa fa-arrow-left"></i></a></button>&nbsp;Lixeira de Menus
                    @else
                        Menus
                    @endif 

                    <div class="pull-right">
                        <button type="button"><a href="{{ url('menus/trash') }}"><i class="fa fa-trash-o"></i></a></button>
                        <button type="button"><a href="{{ url('menus/create') }}"><i class="fa fa-plus"></i></a></button>
                    </div>
                </div>

                @include('menus.list')

                @if(!$trash)
                    <div class="panel-footer">
                        {!! Form::open(['id'=>'destroy-many','method'=>'POST','route'=>'menus.destroy-many']) !!}
                            <div class="mx-2">
                                {!! Form::hidden('menus',null,['id'=>'menus']) !!}
                                {!! Form::submit('Enviar para a lixeira') !!}
                            </div>
                        {!! Form::close() !!}                    
                    </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection