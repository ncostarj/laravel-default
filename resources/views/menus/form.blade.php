@if(isset($menu))
    {!! Form::model($menu, ['method'=>'PUT','route' => ['menus.update',$menu->id],'class'=>'form-horizontal']) !!}
@else
    {!! Form::open(['route'=>'menus.store','class'=>'form-horizontal']) !!}
@endif

    <div class="col-md-12">
        <div class="form-group">
            {!! Form::label('icon','Ícone',['class'=>'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('icon', null, ['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="form-group">            
            {!! Form::label('name','Nome',['class'=>'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('name', null, ['class'=>'form-control']) !!}
            </div>
        </div>

        <div class="form-group">            
            {!! Form::label('location','Link',['class'=>'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('location', null, ['class'=>'form-control']) !!}
            </div>
        </div>

        @if(isset($menu) && $menu->parent_id)
            <?php $parent = $menu->parent_id; ?>            
        @else
            <?php $parent = null; ?>
        @endif
        <div class="form-group">
            {!! Form::label('parent','Menu pai',['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('parent', $parents, $parent, ['class' => 'form-control','placeholder' => 'Nenhum']) !!}
            </div>
        </div>

        <div class="form-group">
            {!! Form::label('status','Status',['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('status', [0 => 'Desabilitado',1 => 'Habilitado'], null, ['class' => 'form-control','placeholder' => 'Selecione']) !!}
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                {!! Form::submit('Salvar', ['class' => 'btn btn-default']) !!}
            </div>
        </div>
    </div>



{!! Form::close() !!}