<div class="form-group">
	<div class="col-md-10 col-md-offset-2">
		@forelse($menus as $menu)
			<?php $checked = ''; ?>
			@if(isset($profile))
	            @forelse($profile->menus as $profile_menu)
	                @if($menu->id == $profile_menu->id)
	                	<?php $checked = 'checked'; ?>
	                @endif
	            @empty
	            @endforelse
            @endif
			<label><input name="menus[]" type="checkbox" value="{{ $menu->id }}" {{ $checked }}>&nbsp;<i class="{{ $menu->icon }}"></i>&nbsp;{{ $menu->name }}</label>
		@empty
		@endforelse
	</div>
</div>