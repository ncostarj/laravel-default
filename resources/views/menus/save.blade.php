@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><button type="button"><a href="{{ url('menus') }}"><i class="fa fa-arrow-left"></i></a></button>&nbsp;Menu</div>

                <div class="panel-body">

                    @include('common.messages')

                    @include('menus.form')
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection