<table class="table">
    <tr>
        @if(!$trash)
        <th><input class="" id="todos" type="checkbox"></th>
        @endif
        <th>Nome</th>
        <th>Link</th>
        <th>Status</th>
        <th>Ações</th>
    </tr>
    @forelse($menus as $menu)
        <tr>
            @if(!$trash)
            <td><input class="ck-item" type="checkbox" value="{{ $menu->id }}"></td>
            @endif
            <td>{{ $menu->name }}</td>
            <td>{{ $menu->location }}</td>
            <td>
                @if($menu->status)
                    <span class="label label-success">Habilitado</span>
                @else
                    <span class="label label-danger">Desabilitado</span>
                @endif
            </td>
            <td>
                @if($trash)
                    <button type="button"><a href="{{ url('menus/'.$menu->id.'/restore') }}" title="Restaurar"><i class="fa fa-reply"></i></a></button>
                    <button type="button"><a href="{{ url('menus/'.$menu->id.'/delete') }}" title="Apagar"><i class="fa fa-trash"></i></a></button>
                @else
                    {!! Form::open(['method'=>'delete','route' => ['menus.destroy',$menu->id]]) !!}
                        <button type="button"><a href="{{ url('menus/'.$menu->id.'/edit') }}" title="Editar"><i class="fa fa-pencil"></i></a></button>
                        <button type="submit"><i class="fa fa-minus"></i></button>
                        @if($menu->status)
                            <button type="button"><a href="{{ url('menus/'.$menu->id.'/deactivate') }}"><i class="fa fa-toggle-off" title="Desabilitar"></i></a></button>
                        @else
                            <button type="button"><a href="{{ url('menus/'.$menu->id.'/activate') }}" title="Habilitar"><i class="fa fa-toggle-on"></i></a></button>
                        @endif
                    {!! Form::close() !!}
                @endif
            </td>
        </tr>
    @empty
        <tr>
            <td colspan="5">Nenhum resultado!</td>
        </tr>
    @endforelse
</table>