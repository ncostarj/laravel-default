<table class="table">
    <tr>
        @if(!$trash)
        <th><input class="" id="todos" type="checkbox"></th>
        @endif
        <th>Nome</th>
        <th>Descrição</th>
        <th>Ações</th>
    </tr>
    @forelse($profiles as $profile)
        <tr>
            @if(!$trash)
            <td><input class="ck-item" type="checkbox" value="{{ $profile->id }}"></td>
            @endif
            <td>{{ $profile->name }}</td>
            <td>{{ $profile->description }}</td>
            <td>
                @if($trash)
                    <button type="button"><a href="{{ url('profiles/'.$profile->id.'/restore') }}"><i class="fa fa-reply"></i></a></button>
                    <button type="button"><a href="{{ url('profiles/'.$profile->id.'/delete') }}"><i class="fa fa-trash"></i></a></button>
                @else
                    {!! Form::open(['method'=>'delete','route' => ['profiles.destroy',$profile->id]]) !!}
                        <button type="button"><a href="{{ url('profiles/'.$profile->id.'/edit') }}"><i class="fa fa-pencil"></i></a></button>
                        <button type="submit"><i class="fa fa-minus"></i></button>
                    {!! Form::close() !!}
                @endif

            </td>
        </tr>
    @empty
        <tr>
            <td colspan="4">Nenhum resultado!</td>
        </tr>
    @endforelse
</table>