@if(isset($profile))
    {!! Form::model($profile, ['method'=>'PUT','route' => ['profiles.update',$profile->id],'class'=>'form-horizontal']) !!}
@else
    {!! Form::open(['route'=>'profiles.store','class'=>'form-horizontal']) !!}
@endif

    <div class="col-md-12">
        <div class="form-group">            
            {!! Form::label('name','Nome',['class'=>'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('name', null, ['class'=>'form-control']) !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('description','Descrição',['class'=>'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::text('description', null, ['class'=>'form-control']) !!}
            </div>
        </div>

        @include('menus.list_menu_profile')

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                {!! Form::submit('Salvar', ['class' => 'btn btn-default']) !!}
            </div>
        </div>
    </div>



{!! Form::close() !!}