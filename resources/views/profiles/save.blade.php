@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><button type="button"><a href="{{ url('profiles') }}"><i class="fa fa-arrow-left"></i></a></button>&nbsp;Perfil</div>

                <div class="panel-body">

                    @include('common.messages')

                    @include('profiles.form')
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection