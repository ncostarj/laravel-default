@extends('layouts.app')
@section('scripts')
    <script type="text/javascript" src="{{ asset('js/profiles.js') }}"></script>
@endsection
@section('content')
<div class="container">

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @include('common.messages')
        </div>
    </div>

    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

				<div class="panel-heading">

                    @if($trash)
                        <button type="button"><a href="{{ url('profiles') }}"><i class="fa fa-arrow-left"></i></a></button>&nbsp;Lixeira de Perfis
                    @else
                        Perfis
                    @endif 

                    <div class="pull-right">
                        <button type="button"><a href="{{ url('profiles/trash') }}"><i class="fa fa-trash-o"></i></a></button>
                        <button type="button"><a href="{{ url('profiles/create') }}"><i class="fa fa-plus"></i></a></button>
                    </div>

                </div>

                @include('profiles.list')

                @if(!$trash)
                <div class="panel-footer">                	
					{!! Form::open(['id'=>'destroy-many','method'=>'POST','route'=>'profiles.destroy-many']) !!}
					    {!! Form::hidden('profiles',null,['id'=>'profiles']) !!}
					    {!! Form::submit('Enviar para a lixeira') !!}
					{!! Form::close() !!}                	
                </div>
                @endif

            </div>
        </div>
    </div>
</div>
@endsection