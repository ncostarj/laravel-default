<html>
<head>
</head>
<body>
	<h1>Olá, {{ $user->name }}</h1>
	<p>
		Você recebeu este email porque foi pedido uma redefinição de senha para sua conta.<br/>
		<a href="{{ url('/password/reset/'. $token) }}">Redefinir senha</a><br/>
		Se você não fez o pedido de redefinição de senha, por favor desconsidere este email.
	</p>
	<p>
		Atenciosamente,<br/>
		Equipe Site.
	</p>
	<p><small>© 2017 Site. Todos os direitos reservados.</small></p>
</body>
</html>