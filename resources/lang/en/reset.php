<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during reset for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title'        => 'Reset Password',
    'email'        => 'E-mail Address',
    'password'     => 'Password',
    'confirmation' => 'Password Confirmation',
    'action'       => 'Reset Password',
];