<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Registration Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during registration for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'hello'     => 'Hello',
    'line1'     => 'You are receiving this email because we received a password reset request for your account.',
    'link'      => 'Reset Password',
    'errata'    => 'If you did not request a password reset, no further action is required.',
    'greetings' => 'Regards,',
    'signature' => 'Site',
    'copyright' => '© 2017 Site. All rights reserved.',

];