<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Registration Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during registration for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title'        => 'Cadastro',
    'name'         => 'Nome',
    'email'        => 'Email',
    'password'     => 'Senha',
    'confirmation' => 'Confirmação de Senha',
    'action'       => 'Cadastrar',

];