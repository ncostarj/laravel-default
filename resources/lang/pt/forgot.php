<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during email for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'subject'  => 'Redefinição de senha',
    'title'    => 'Redefinir senha',
    'email'    => 'Email',
    'action'   => 'Enviar link de redefinição',
];