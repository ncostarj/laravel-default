<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Registration Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during registration for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'hello'     => 'Olá',
    'line1'     => 'Você recebeu este email porque foi pedido uma redefinição de senha para sua conta.',
    'link'      => 'Redefinir senha',
    'errata'    => 'Se você não fez o pedido de redefinição de senha, por favor desconsidere este email.',
    'greetings' => 'Atenciosamente,',
    'signature' => 'Equipe Site.',
    'copyright' => '© 2017 Site. Todos os direitos reservados.',

];